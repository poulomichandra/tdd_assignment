class TemperatureConverter:

    def __init__(self):
        self.temp = -40

    def add_temp(self, temp):
        if type(temp) == str:
            raise Exception("Numeric inputs only")
        self.temp = temp

    def fahrenheit_to_celsius(self):
        celsius_temp = (self.temp - 32) * 5 / 9
        return round(celsius_temp, 2)

    def celsius_to_fahrenheit(self):
        fahreneit_temp = (9 * self.temp / 5) + 32
        return round(fahreneit_temp, 2)

    def readFromFile(self, filename):
        infile = open(filename, "r")
        line = infile.readline()
        return line

