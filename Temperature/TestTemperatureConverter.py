import pytest
from unittest.mock import MagicMock
from Temperature.TemperatureConverter import TemperatureConverter


class TestClass:

    @classmethod
    def setup_class(cls):
        print("\nSetting Up Module")

    @classmethod
    def teardown_class(cls):
        print("\nTearing Down Module")

    @pytest.fixture()
    def temperature(self):
        temperature = TemperatureConverter()
        temperature.add_temp(156)
        return temperature

    @pytest.mark.parametrize("temp, result", [(-40, -40), (25, 77), (70, 158)])
    def test_convert_celsius_to_fahrenheit(self, temperature, temp, result):
        temperature.add_temp(temp)
        assert temperature.celsius_to_fahrenheit() == result

    def test_convert_fahrenheit_to_celsius(self, temperature):
        assert temperature.fahrenheit_to_celsius() == 68.89

    def test_exception_for_wrong_input(self, temperature):
        with pytest.raises(Exception):
            temperature.add_temp("abc")

    def test_return(self, temperature, monkeypatch):
        mock_file = MagicMock()
        mock_file.readline = MagicMock(return_value="-40")
        mock_open = MagicMock(return_value=mock_file)
        monkeypatch.setattr("builtins.open", mock_open)
        result = temperature.readFromFile("input_file")
        mock_open.assert_called_once_with("input_file", "r")
        assert result == "-40"